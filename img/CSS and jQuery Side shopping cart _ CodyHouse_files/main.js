jQuery(document).ready(function($){
	//open hamburger menu on mobile
	$('.hamburger-menu').on('click', function(){
		$('header').toggleClass('nav-is-visible');
	});

	//fix socials/demo bar on scrolling
	if ($('#single-product-bar').length > 0) {
		$('.prev-next').addClass('is-hidden');
		$(window).load(function(){
			var barTop = $('#single-product-bar .cd-container').offset().top;
			fixSocialBar(barTop);
			$(window).scroll(function() {
				(!window.requestAnimationFrame) 
					? fixSocialBar(barTop) 
					: window.requestAnimationFrame(function(){ fixSocialBar(barTop) });
			});
		});
	}

	function fixSocialBar(barTop) {
		if( $(window).scrollTop() >= barTop) {
			$('#single-product-bar').addClass('is-fixed');
			$('.prev-next').removeClass('is-hidden');
		} else {
			$('#single-product-bar').removeClass('is-fixed');
			$('.prev-next').addClass('is-hidden');
		}
	}

	//smooth scrolling
	$('main .date a[data-type="comments-nb"]').on('click', function(e){
        e.preventDefault();
        var target= $('#comments');
        $('body,html').animate(
            {'scrollTop':target.offset().top},
            900
        ); 
	});

	
	// var $msxg_section= $('section#msg'),
		// $gem_download_id= $('#downloaded-gem').data('id');

	// $msg_section.append("<iframe class='redirect' src='http://localhost:8888/cody-1-0/?download="+$gem_download_id+"' height='0' width='0' style='display:none'></iframe>");
	// var $redirect_iframe= $('iframe.redirect');
	
	//remove iframe
	var downloadURL = function downloadURL(url) {
	    var hiddenIFrameID = 'hiddenDownloader',
	        iframe = document.getElementById(hiddenIFrameID);
	    if (iframe === null) {
	        iframe = document.createElement('iframe');
	        iframe.id = hiddenIFrameID;
	        iframe.style.display = 'none';
	        document.body.appendChild(iframe);
	    }
	    iframe.src = url;
	};

	if( $('span.redirect').length > 0 ) {
		var download_url=$('span.redirect').data('type');
		downloadURL(download_url);
	}

	//browse gallery with keyboard
	if( $('#pagination').length > 0 ) {
		$(document).keyup(function(event){
			if( event.which == 39 ) {
				cdNextPage();
			} else if( event.which == 37 ) {
				cdPrevPage();
			}
		});
	}

	function cdNextPage() {
		var nextAnchor = $('#pagination .next a');
		if ( nextAnchor.length > 0 ) {
			var newPage = nextAnchor.attr('href');
			document.location.href = newPage;
		}
	}

	function cdPrevPage() {
		var nextAnchor = $('#pagination .prev a');
		if ( nextAnchor.length > 0 ) {
			var newPage = nextAnchor.attr('href');
			document.location.href = newPage;
		}
	}
});